type Result2 = { periodLength: number, trainingDays: number, success: boolean, rating: number, ratingDescription: string, target: number, average: number };
export const calculateExercise = ( exerciseHoursArray: Array<number>, target: number) : Result2 => {
    return { periodLength: exerciseHoursArray.length,
        trainingDays: exerciseHoursArray.filter((daily: number) => daily > 0).length,
        success: false,
        rating: 2,
        ratingDescription: 'not too bad but could be better',
        target,
        average: exerciseHoursArray.reduce((acc: number, cur: number) => acc+cur) / exerciseHoursArray.length
    };
};

//console.log(calculateExercise([3, 0, 2, 4.5, 0, 3, 1], 2));