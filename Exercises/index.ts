import express from 'express';
import {calculateBmi} from "./bmiCalculator";
import { calculateExercise } from './exerciseCalculator';

//const express = require('express');
const app = express();
app.use(express.json());

app.get('/hello', (_req, res) => {
  res.send('Hello full stack');
});

app.get('/bmi', (req, res) => {
  const height = Number(req.query.height);    
  const weight = Number(req.query.weight);
  if (Number.isNaN(height) || Number.isNaN(weight)) {
      const retVal = {
          error: "malformatted parameters"
      };

      return res.json(retVal);
  } else {
      const calculatedBmi = calculateBmi(Number(height), Number(weight));
      return res.json(calculatedBmi);
  }
});

app.post('/calculate', (req, res) => {
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  const arr = Array<number>(req.body.exerciseHoursArray);
  // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
  const tar = Number(req.body.target);
  const isNotArray = !Array.isArray(arr);
  const hasNaNs = arr.filter((el: number) => Number.isNaN(Number(el))).length > 0;
  const isNotANumber = Number.isNaN(tar);

  if (isNotArray || hasNaNs || isNotANumber) {
    const retVal = {
        error: "malformatted parameters"
    };
    return res.json(retVal);
  } else {
    const retExercise = calculateExercise(arr, tar);
    console.log(retExercise);
    return res.json(retExercise);
  }
});

const PORT = 3003;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});