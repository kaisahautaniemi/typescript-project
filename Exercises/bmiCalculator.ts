type Result = {height: number, weight: number, bmi: string};

export const calculateBmi = (height: number, weight: number) : Result => {
    const bmi = Math.round(weight/(height/100 * height/100));
    let bmiDesc = "";
    switch (bmi){
    case 18:
    case 19:
    case 20:
    case 21:
    case 22:
    case 23:
    case 24: 
    case 25:
        bmiDesc = "Normal (healthy weight)";
        break;
    default:
        bmiDesc =  "Under or overweight";
        break;
    }
    return { height,
        weight,
        bmi: bmiDesc
    };
};

//console.log(calculateBmi(180, 74));