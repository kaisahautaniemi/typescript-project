import express from 'express';
import fs from "fs";

const app = express();
app.use(express.json());

const PORT = 3001;

app.get('/ping', (_req, res) => {
  console.log('someone pinged here');
  res.send('pong');
});

interface DiagnosesConfig {
  code: string;
  name: string;
  latin?: string;
}
type ResultDiagnoses = { code: string, name: string, latin?: string};
const returnDiagnoses = (diags: DiagnosesConfig) : ResultDiagnoses => {
  return diags;
};
app.get('/api/diagnoses', (_req, res) => {
  const data = fs.readFileSync("./data/diagnoses.json", "utf8");
  const retVal : ResultDiagnoses = returnDiagnoses(JSON.parse(data));
  return res.json(retVal);
});

interface PatientsConfig {
  "id": string,
  "name": string,
  "dateOfBirth": string,
  "ssn": string,
  "gender": string,
  "occupation": string
}
type ResultPatients = {
  "id": string,
  "name": string,
  "dateOfBirth": string,
  "gender": string,
  "occupation": string
};
const returnPatients = (pats: Omit<PatientsConfig, "ssn">) : ResultPatients => {
  const retVal: ResultPatients = pats;
  return retVal;
};
app.get('/api/patients', (_req, res) => {
  const data = fs.readFileSync("./data/patients.json", "utf8");
  const retVal = returnPatients(JSON.parse(data));
  console.log(retVal);
  return res.json(retVal);
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});